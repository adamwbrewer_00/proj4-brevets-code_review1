# Project 4: Brevet time calculator with Ajax

This page is a version of RUSA ACP controle time calculator with flask and ajax.

- Credits to Michal Young for the initial version of this code.
- Base verison given by Prof. Ram Durairajan
- Final implementation by Thomas Jessop

## ACP control times

 Controls are points where over the course of a brevet a rider must obtain proof of passage, and control times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Note the 1000-1300 row is not impementated for it is not an official distance.

Special edgecases: The close time for a control at 0km is set by rules and one hour. Similarly the end if the control and the brevet are both at 200km the time is set by rules at 13h30m.

## Testing
The internal logic calculating the control open and close times are tested with nose unit testing.

Said tests can be run by 
    `cd {project parrent directory}/proj4-brevets/brevets
	nosestests`

## Repo
[Bitbucket proj4-brevets](https://bitbucket.org/Tljessop/proj4-brevets/src/master/)

## Maintainer
Thomas Jessop
<tjessop2@uoregon.edu>